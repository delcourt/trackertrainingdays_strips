from ROOT import TFile, TH1F, TEfficiency, kRed
import sys, os, argparse, glob
from math import cos
from common import *

to_process = glob.glob("/eos/cms/store/group/dpg_tracker_strip/comm_tracker/Strip/Calibration/calibrationtree/GR22/calibTree_361971_*.root")

modules_ref      = load_modules("TIB_minus_4_3_1.txt")
modules_uncooled = load_modules("uncooled_modules.txt")

hist_cooled   = TH1F("hist_cooled","hist_cooled",200,0,2000)
hist_uncooled = TH1F("hist_uncooled","hist_uncooled",200,0,2000)
n_clust        = 0
n_max_clusters = 10000

for f_id, f_name in enumerate(to_process):
    print(f"Opening file {f_id+1}/{len(to_process)} : {f_name}. {100.*n_clust/n_max_clusters}% filled")
    ff = TFile(f_name)
    charge_tree = ff.Get("gainCalibrationTreeStdBunch/tree")
    for event in charge_tree:
        for detId, Charge, Path in zip(event.GainCalibrationrawid, event.GainCalibrationcharge, event.GainCalibrationpath):
            if get_subdet(detId) != 3 or get_layer(detId) != 4:
                continue
            if detId in modules_uncooled:
                hist_uncooled.Fill(Charge/Path)
                n_clust+=1
            elif detId in modules_ref:
                hist_cooled.Fill(Charge/Path)
            if n_clust > n_max_clusters:
                break
        if n_clust > n_max_clusters:
            break
    if n_clust > n_max_clusters:
        break

of = TFile("dedx_comparison.root","RECREATE")
hist_cooled.Scale(1./hist_cooled.Integral())
hist_uncooled.Scale(1./hist_uncooled.Integral())
hist_uncooled.Write()
hist_cooled.Write()
of.Close()