from ROOT import TFile

ct = TFile("/eos/cms/store/group/dpg_tracker_strip/comm_tracker/Strip/Calibration/calibrationtree/GR18/calibTree_319579_2025.root")

charge_tree = ct.Get("gainCalibrationTreeStdBunch/tree")
eff_tree    = ct.Get("anEff/traj")
la_tree     = ct.Get("lorentzAngleRunInfo/tree")
info_tree   = ct.Get("eventInfo/tree")

print("Content of the  'Gain Calibration tree':")
charge_tree.Print()
print("\n\n")
print("Content of the  'Efficiency tree':")
eff_tree.Print()
print("\n\n")
print("Content of the  'event information tree':")
info_tree.Print()