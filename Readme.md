# Tracker training days - SiStrip hands-on session
## Introduction
Thank you for choosing the strips local reconstruction and calibration exercice session!

During the following hours, we will try to have a bit of a feel of the calibration data and what we can do with it. To do so, I suggest you try to set your own objectives based on the data available and your personnal interest.

This document will quickly explain a few things about the Calibration Trees and how to read them.

## Calibration Trees?
As was just explained, the calibration trees are produced as to store data that can be used to study the Sistrip tracker. They are stored in the following directory:

```
/eos/cms/store/group/dpg_tracker_strip/comm_tracker/Strip/Calibration/calibrationtree/
```

As far as the exercice is concerned, the following sub-directories are of interest:

- GR22      : Calibration trees 2022 collisions
- GR22_Aag  : Calibration trees 2022 collisions, using "After the Abort Gap" data
- GR22_CKF  : Run2022F reprocessed with the CKF algorithm (for hitEff)
- GR18      : Calibration trees 2018 collisions, for historical perspective

With the notable exception of the CKF reprocessed data, the Calibration trees are split by run number and by lumi-section (up to 25 LS are aggregated in a tree).

## Structure of the trees
A full description is available here :
https://twiki.cern.ch/twiki/bin/view/CMS/SiStripGainCalibration#Production_of_the_calibration_tr

Since it's a bit painful to read a full twiki for a short exercice, here's what relevant for us:
### eventInfo/tree
This tree provides contextual information on the event. Each entry is a single event and carries the following information:

| Branch | Description |
| ----------- | ----------- |
| `PU`        | Estimated PU of the event |
| `instLumi`  | Estimated instLumi of the event |
| `bx`        | Bx position in the LHC orbit |
| `event`     | Event number |

Given the small amount of events contained in a tree, this one can usually be loaded directly in memory when needed when analysing the other trees. For example, this way to get `PU` information:

``` python
from ROOT import TFile
to_process = "/eos/cms/store/group/dpg_tracker_strip/comm_tracker/"
            +"Strip/Calibration/calibrationtree/GR22/calibTree_361971_25.root"

ff = TFile(f_name)
info_tree   = ff.Get("eventInfo/tree")
PU_dic = {}
for entry in info_tree:
    PU_dic[entry.event] = entry.PU
```

### gainCalibrationTreeStdBunch/tree or gainCalibrationTreeAagBunch/tree

These trees contain the cluster charges and relevent information to calibrate G2. It also has one entry per event, leading most of its branches to be vectors, with one entry per cluster.

Along these vectors, the index is conserved, meaning that a given index `i` corresponds to the same cluster in all vectors.

Some of the branches are the following:

| Branch | Description |
| ----------- | ----------- |
| `GainCalibrationgainused`        | G2 used during reconstruction |
| `GainCalibrationgainusedTick`  | G1 used during reconstruction |
| `GainCalibrationpath`        | Expected path length in the Si |
| `GainCalibrationcharge`     | Cluster Charge measured |
| `GainCalibrationrawid`     | Detector id |

Make sure you read the correct tree for a given set of bunches (`gainCalibrationTreeAagBunch` if the folder of your calibration tree specify "Aag", `gainCalibrationTreeStdBunch` if not.)

Since we are looking at reading different vectors with the same index, we can use `zip`

```python
from ROOT import TFile
to_process = "/eos/cms/store/group/dpg_tracker_strip/comm_tracker/"
            +"Strip/Calibration/calibrationtree/GR22/calibTree_361971_25.root"

ff = TFile(to_process)
charge_tree   = ff.Get("gainCalibrationTreeStdBunch/tree")

for entry in charge_tree:
    charge = entry.GainCalibrationcharge
    path   = entry.GainCalibrationpath
    detId  = entry.GainCalibrationrawid

    for (ch,pa,di) in zip(charge,path,detId):
        print(f"Cluster charge : {ch}, path: {pa}, detId: {di}")

    # Let's not print the full tree now...
    break
```

### anEff/traj

Last but not least, the efficiency tree. This one differs from the others because it features **an entry per cluster** and not per event.

The most relevant components are the following:

| Branch | Description |
| ----------- | ----------- |
| ` TrajLocAngleX`        | Traj Local X angle (perpendicular to the strips ) |
| ` TrajLocAngleY`  | Traj Local Y angle (along the strips)|
| `ClusterStoN`        | Cluster Signal-To-Noise Ratio |
| `ModIsBad`     | Bad module flag |
| `SiStripQualBad`     | Another bad module flag |
| `trajHitValid`     | The expected hit was found |
| `event`     | event number |
| `Id`     | module detId |


## A few tips

### glob

The glob module in python offers a nifty way of getting all files corresponding to a wildcard, which is great for calibration trees!

For example, if I wanted all trees for the 2022 run 361971, I can just go:

``` python
from glob import glob
ctList = glob("/eos/cms/store/group/dpg_tracker_strip/comm_tracker/Strip/Calibration/calibrationtree/GR22/calibTree_361971_*.root")
```

### detId to subDet/Layer

The detId, hidding behind its very long number, provides some information on the geometrical location of the module. Everything is readily available in CMSSW, but you can also just use these functions, I *think* they aren't bugged:

``` python
from common import get_name, get_subdet, get_layer
print(get_name(369169636))   # TIB Layer 3
print(get_layer(369169636))  # Returns the number of the layer/wheel
print(get_subdet(369169636)) # 0,1,2 -- not SiStrip
                             # 3,4,5,6 = TIB,TID,TOB,TEC
```

### Cabling

Erik has this nice webpage to get lists of detId/power groups, feds and whatnot:

https://test-ebutz.web.cern.ch/test-ebutz/cgi-bin/tkCabling_dev_dt.pl

### Two list of modules

In the following files, you have a list of passively cooled modules (in `uncooled_modules.txt`) that correspond to the power group TIBplus_4_3_1. 

Since the detector should be roughly symmetric to a z -> -z transformation, here's the corresponding power group on the other side of the tracker: `TIB_minus_4_3_1.txt`. They can provide a reference to compare the uncooled modules to.

To load them in you script, either parse the files yourself, or use the following helper function:

``` python
from common import load_modules
my_modules = load_modules("TIB_minus_4_3_1.txt")
```

### TEfficiency
If you want to compute an efficiency, ROOT has you covered! Book it as a histogram, and fill by starting with weither your numerator was true or not.

Example for an efficiency of 60% at x = 10:

``` python
from ROOT import TEfficiency
eff = TEfficiency("My_efficiency","My_efficiency",100,0,100)

eff.Fill(0,10)
eff.Fill(1,10)
eff.Fill(1,10)
eff.Fill(1,10)
eff.Fill(0,10)

eff.Draw()
```

It's just more painful to work with afterwards while plotting...


## Measurement suggestions

### dEdx
A "fun" little calibration you might want to re-produce is that of the particle gain. While this parameter is being tuned for each APV in the tracker, we can here just look at bigger structures, power groups or whole layers for example.

- You can first try to extract the overall distribution for a power group/layer, and try to fit a landau (`histogram.Fit("landau")`). You should soon realise that it doesn't work very well... To really get the MPV you will want to either only fit the peak itself, playing with the ranges, or fit a landau*gaus convolution.

- Charge collected in passively cooled modules : As explained on Monday by Ivan, two cooling loops were turned off in the beginning of Run-3. An interesting exercice would be to study the impact of this on the charge collected in the detector. In particular, the list of uncooled modules in the TIB L4 is given in the file `uncooled.txt`. To have a somewhat comparable set of modules, their conterpart through a z->-z transformation are given in the file `TIB_minus_4_3_1.txt`. Before you start, you might also think about weither you expect any difference

- Evolution of uncalibrated charge : By multiplying the collected charge by both G1 (tickmark) and G2 (Particle), you can extract the raw uncalibrated charge collected. You could look and how the calibrations change the charge collected, or how this evolves with time

- After Abort Gaps : Collisions recorded right after the Abort Gap of the LHC are stored in specific files & trees. You can find them in GR22_Aag, in the tree `gainCalibrationTreeAagBunch`. By comparing the same run with the twi different bunches, the impact of Heavy Ionising Particles on the collected charge can be seen.


### Signal-To-Noise
The signal-To-Noise ratio is a crucial value to monitor since it will directly impact the cluster reconstruction (thus hit efficiency), but also the resolution of the tracker.

A possible list of items to study would be the following:

- path length correction : Since the signal will depend on the pathlength of particles inside the silicon, one might want to correct the measured Signal-To-Noise to take this effect into account. Given Rx and Ry the local angles in the x and y directions, a back of the boarding pass calculation tells us that the length travelled in the silicon is L = w / cos Rx cos Ry. The correction factor for the Signal-To-Noise should therefore look like cos Rx * cos Ry.

- S/N in passively cooled modules : As explained on Monday by Ivan, two cooling loops were turned off in the beginning of Run-3. An interesting exercice would be to study the impact of this on the S/N in the detector. In particular, the list of uncooled modules in the TIB L4 is given in the file `uncooled.txt`. To have a somewhat comparable set of modules, their conterpart through a z->-z transformation are given in the file `TIB_minus_4_3_1.txt`.

- With these awesome distributions in hand, one could even look at trends throughout 2022 data. To do so, you can extract the maximum of these curves through your desired method (get the max of the histogram, of fit the peak if you feel fancy). Then, go through some runs in the `GR22` and see how these values and their ratio change.


### Hit Efficiency

While the full hit efficiency measurement is a [quite a bit trickier](https://github.com/cms-sw/cmssw/blob/master/CalibTracker/SiStripHitEfficiency/plugins/SiStripHitEffFromCalibTree.cc), a first estimate can be taken by looking at missing hits in tracks, without any other requirement. For a real measurement, tighter track and cluster selection is applied, and missed clusters are recovered.

When building the calibration trees, the anEff tree is filled with clusters by looping on tracks. If no cluster is found, the expected hit is still stored, but the `trajHitValid` is set as `false`.

Beware that if you use calibration trees from 2022 that are not from the "CKF" directory, you might be biasing your result.

- One can try to reproduce a first hit efficiency by looking at the ratio of valid hits in a given sub-detector, layer, etc.

- The efficiency can also be measured as a function of pile-up or instantaneous luminosity (see the eventInfo tree). To do this, you might want to run on multiple calibration trees to have a bigger range of Pile-Up, for example, on all trees of run 319579 of GR18. A cut on the number of clusters analysed per file can be added to speed up the process. You can ask yourself the question as to why the slope in the TOB is so much bigger than in other layers.

- Efficiency in passively cooled modules : As explained on Monday by Ivan, two cooling loops were turned off in the beginning of Run-3. An interesting exercice would be to study the impact of this on the hit efficiency of the detector. In particular, the list of uncooled modules in the TIB L4 is given in the file `uncooled.txt`. To have a somewhat comparable set of modules, their conterpart through a z->-z transformation are given in the file `TIB_minus_4_3_1.txt`.



## Examples
The following examples might help you:
- `plot_SN.py`    shows how to get the S/N plot shown in my slides
- `plot_Charge_Over_Path.py`  shows how to get the dedx plot shown in my slides
- `plot_Eff.py`   shows how to get the efficiency plot shown in my slides
- `dump_skeleton.py`    shows the content of a calibration tree