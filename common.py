
def get_name(detId):
    return ["","","","TIB","TID","TOB","TEC"][get_subdet(detId)]+f" Layer {get_layer(detId)}"

def get_subdet(detId):
    return((detId>>25)&0x7)

def get_layer(detId):
    return (detId>>14)&0x7

def load_modules(module_file):
    mod_list = []
    with open(module_file,"r") as f:
        for line in f:
            line = line.replace("\n","")
            if not line.isdigit():
                print(f"Ignoring line : {line}")
            else:
                mod_list.append(int(line))
    return mod_list
