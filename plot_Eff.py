from ROOT import TFile, TH1I, TEfficiency
from common import *
import glob
to_process = glob.glob("/eos/cms/store/group/dpg_tracker_strip/comm_tracker/Strip/Calibration/calibrationtree/GR18/calibTree_319579_*.root")

eff     = TEfficiency("eff_vs_PU","my efficiency;PU;eff;",20,0,120)
eff_bx  = TEfficiency("eff_vs_BX","my efficiency;BX;eff;",200,0,3000)
n_clust = 0

for f_id, f_name in enumerate(to_process):
    print(f"Opening file {f_id+1}/{len(to_process)} : {f_name}")
    ff = TFile(f_name)
    eff_tree = ff.Get("anEff").Get("traj")

    info_tree   = ff.Get("eventInfo/tree")
    PU_dic = {}
    BX_dic = {}
    for entry in info_tree:
        PU_dic[entry.event] = entry.PU
        BX_dic[entry.event] = entry.bx

    for entry in eff_tree:
        if get_subdet(entry.Id) != 5 or get_layer(entry.Id) != 1:
            continue
        if entry.ModIsBad or entry.SiStripQualBad:
            continue
        eff.Fill(entry.trajHitValid,PU_dic[entry.event])
        eff_bx.Fill(entry.trajHitValid,BX_dic[entry.event])


of = TFile("test_eff.root","RECREATE")
eff.Write()
eff_bx.Write()
of.Close()

